package com.android.SAXParser;

import java.net.URL;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

public class SAXParserActivity extends Activity {

	private XMLGettersSetters data;
	private ListView news;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/**
		 * Layout for Main activity
		 **/
		setContentView(R.layout.main);
		downloadUrl();
	}

	private void downloadUrl() {

		try {
			news = (ListView) findViewById(R.id.listView1);
			/**
			 * Create a new instance of the SAX parser
			 **/
			SAXParserFactory saxPF = SAXParserFactory.newInstance();
			SAXParser saxP = saxPF.newSAXParser();
			XMLReader xmlR = saxP.getXMLReader();
			/**
			 * URL of the Feeds
			 **/
			URL url = new URL("http://feeds.bbci.co.uk/news/technology/rss.xml");

			/**
			 * Create the Handler to handle each of the XML tags.
			 **/
			XMLHandler myXMLHandler = new XMLHandler();
			xmlR.setContentHandler(myXMLHandler);
			xmlR.parse(new InputSource(url.openStream()));
			displayFeeds();
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	private void displayFeeds() {
		data = XMLHandler.data;
		Log.v("count", "Total Values are:" + data.getTitle().size());
		/**
		 * Splitting each news
		 **/
		String item[] = data.b.toString().split("END");
		news.setAdapter(new CustomAdapter(SAXParserActivity.this, item));
		/**
		 * Logging for reference
		 **/
		for (int i = 0; i < item.length; i++) {
			Log.v("ITEM" + (i + 1), item[i]);
		}

	}
}
